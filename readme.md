<!--
- SPDX-License-Identifier: CC-BY-SA-4.0
- Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
-->
# protostack/compiler

protostack is a set of docker containers for C++ compilers. Each compiler has one container
associated with it. Extra utilities like coverage tools, cmake and git are installed along with the
compilers. We generally try to support compilers upto 3 years old.
Currently supported compilers are




| Compiler | Version | Docker Image | Status |
|----------|---------|--------------|--------|
| GCC      | 13.2.0   | registry.gitlab.io/protostack/compiler/gcc:13.2.0 | :white_check_mark: |
| GCC      | 12.3.0   | registry.gitlab.io/protostack/compiler/gcc:12.3.0 | :x: |
| GCC      | 11.4.0   | registry.gitlab.io/protostack/compiler/gcc:11.4.0 | :x: |
|  | |  |  |
| LLVM | 17.0.6   | registry.gitlab.io/protostack/compiler/gcc:17.0.6 | :white_check_mark: |
| LLVM | 16.0.4   | registry.gitlab.io/protostack/compiler/gcc:16.0.4 | :x: |


