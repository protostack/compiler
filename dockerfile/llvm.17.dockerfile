# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
FROM ubuntu:22.04
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install wget lcov gcovr git  lsb-release wget software-properties-common gnupg make -y
COPY src /tmp/src
RUN  /tmp/src/bin/cmake.sh --skip-license --prefix=/usr/local
RUN wget https://apt.llvm.org/llvm.sh && chmod +x llvm.sh && ./llvm.sh 17
RUN update-alternatives --install /usr/bin/clang clang /usr/bin/clang-17 100
RUN update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-17 100

