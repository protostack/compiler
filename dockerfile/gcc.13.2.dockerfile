# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
FROM gcc:13.2.0
RUN apt-get update && apt-get install wget lcov gcovr clang-format git -y
COPY src /tmp/src
RUN  /tmp/src/bin/cmake.sh --skip-license --prefix=/usr/local

